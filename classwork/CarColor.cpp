#include "CarColor.h"

CarColor::CarColor()
{
	this->_color = Color::WHITE; // used 'this' to access objects memebers.
}

// uses initialization line
CarColor::CarColor(const Color& color) : _color(color) {}

Color CarColor::getColor() const
{
	return this->_color; // used 'this' to access objects memebers.
}

// comparison operator
bool CarColor::operator==(const CarColor &other) const
{
	return this->_color == other._color;
}

std::ostream &operator<<(std::ostream &os, const CarColor &carColor)
{
	switch (carColor.getColor())
	{
	case RED:
		os << "red";
		break;
	case BLUE:
		os << "blue";
		break;
	case GREEN:
		os << "green";
		break;
	case YELLOW:
		os << "yellow";
		break;
	case ORANGE:
		os << "orange";
		break;
	case PURPLE:
		os << "purple";
		break;
	case PINK:
		os << "pink";
		break;
	case BLACK:
		os << "black";
		break;
	case WHITE:
		os << "white";
		break;
	case SILVER:
		os << "silver";
		break;
	}
	return os;
}