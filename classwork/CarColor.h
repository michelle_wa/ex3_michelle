#include <iostream>

enum Color
{
	RED = 1,
	BLUE = 2,
	GREEN = 3,
	YELLOW = 4,
	ORANGE = 5,
	PURPLE = 6,
	PINK = 7,
	BLACK = 8,
	WHITE = 9,
	SILVER = 10,
};

class CarColor
{
public:

	CarColor(); // consturcts a CarColor object with default Color
	CarColor(const Color&); // constructs a CarColor object with the given Color
	Color getColor() const; // returns the Color

							// comparison operator
	bool operator==(const CarColor& other) const;

	// one way to enable CarColor print is to override the stream insertion operator '<<' 
	friend std::ostream &operator<<(std::ostream &os, const CarColor &carColor);

private:
	Color _color;
};