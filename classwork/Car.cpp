#include "Car.h"

using std::string;

// initialize a static member
int Car::_totalCars = 0;

// using initialization line to initialize members
Car::Car(string owner, double price, Color color, string model, string company) :
	_owner(owner),
	_price(price),
	_carColor(color),
	_model(model),
	_company(company)
{
	_totalCars++;
}

string Car::getOwner() const { return this->_owner; }
double Car::getPrice() const { return this->_price; }
CarColor Car::getCarColor() const { return this->_carColor; }
string Car::getModel() const { return this->_model; }
string Car::getCompany() const { return this->_company; }

void Car::getOwner(const string newOwner) { this->_owner = newOwner; }
void Car::getPrice(const double newPrice) { this->_price = newPrice; }
void Car::getCarColor(const CarColor newColor) { this->_carColor = newColor; }
void Car::getModel(const string newModel) { this->_model = newModel; }
void Car::getCompany(const string newCompany) { this->_company = newCompany; }

std::ostream &operator<<(std::ostream &os, const Car &car)
{
	os << "Model: " << car.getModel() << "\n"
		<< "Company: " << car.getCompany() << "\n"
		<< "Price: " << car.getPrice() << "$\n"
		<< "Owner: " << car.getOwner() << "\n";
	return os;
}

void Car::print() const
{
	std::cout << *this; // using '<<' (stream insertion) operator
}

bool Car::operator==(const Car &other) const
{
	return this->_price == other._price && this->_model == other._model
		&& this->_company == other._company && this->_carColor == other._carColor;
}

bool Car::operator<(const Car &other) const
{
	if (this->_price == other._price)
	{
		return this->_model < other._model; // using std::string operator<;
	}
	else
	{
		return this->_price < other._price;
	}
}

bool Car::operator>(const Car &other) const
{
	if (this->_price == other._price)
	{
		return this->_model > other._model; // using std::string operator<;
	}
	else
	{
		return this->_price > other._price;
	}
}

bool Car::operator!=(const Car &other) const
{
	return *this != other;
}

int Car::totalCars() const { return _totalCars; }