#include "CarColor.h"
#include <string>

class Car
{
public:

	// Constructors
	Car(std::string owner, double price, Color color, std::string model, std::string company);

	// getters
	std::string getOwner() const;
	double getPrice() const;
	CarColor getCarColor() const;
	std::string getModel() const;
	std::string getCompany() const;

	// setters
	void getOwner(const std::string newOwner);
	void getPrice(const double newPrice);
	void getCarColor(const CarColor newColor);
	void getModel(const std::string newModel);
	void getCompany(const std::string newCompany);

	// operators
	bool operator==(const Car &other) const;
	bool operator<(const Car &other) const;
	bool operator>(const Car &other) const;
	bool operator!=(const Car &other) const;

	// static member getter
	int totalCars() const;

	// print method and operator
	void print() const;
	friend std::ostream &operator<<(std::ostream &os, const Car &car);

private:
	std::string _owner;
	double _price;
	CarColor _carColor;
	std::string _model;
	std::string _company;
	static int _totalCars;
};