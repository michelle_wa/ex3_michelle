#include <iostream>
#include "Vector.h"


/*initialization*/
Vector::Vector(int n)
{
	if (n < 2)
	{
		this->_elements = new int[2];
	}
	else
	{
		this->_elements = new int[n];
	}
	for (int i = 0; i < n; i++)
	{
		this->_elements[i] = 0;
	}
	this->_capacity = n;
	this->_resizeFactor = n;
	this->_size = 0;
}
/*Releases memory*/
Vector::~Vector()
{
	delete[] _elements;
	_elements = nullptr;
}

/*Returns the size*/
int Vector::size() const { return this->_size; }

/*Returns the capacity*/
int Vector::capacity() const {return this->_capacity;}

/*Returns the resizeFactor*/
int Vector::resizeFactor() const {return this->_resizeFactor;}

/*Returns whether the vector is empty*/
bool Vector::empty() const
{
	if (this->_size == 0)
	{
		return true;
	}
	return false;
}


/*Adds an organ at the end of the vector*/
void Vector::push_back(const int& val)
{
	if (this->_size < this->_capacity)
	{
		this->_elements[this->_size] = val;
	}
	else //size >= this->_capacity
	{
		int* copy_arr = new int[this->_capacity + this->_resizeFactor];
		for (int i = 0; i < this->_size; i++)
		{
			copy_arr[i] = this->_elements[i];
		}
		delete[] this->_elements;
		this->_elements = nullptr;
		this->_elements = copy_arr;
		this->_capacity = this->_size + this->_resizeFactor;
	}
	this->_size++;
}
/*Deletes the last accessible part of the vector and returns it. If the null vector returns -9999*/
int Vector::pop_back()
{
	int delete_element = -9999;
	if (this->empty() == false)
	{
		this->_size--;
		delete_element = this->_elements[this->_size];
		this->_elements[this->_size] = 0;
	}
	else  //return -9999 if the elements are empty
	{
		std::cerr << "error: pop from empty vector" << std::endl;
	}
	return delete_element;

}
/*Ensures that the vector capacity is at least n*/
void Vector::reserve(int n)
{
	if (this->_capacity < n)
	{
		while (this->_capacity < n)
		{
			this->_capacity += this->_resizeFactor;
		}
		int* copy_arr = new int[this->_capacity];
		for (int i = 0; i < this->_size; i++)
		{
			copy_arr[i] = this->_elements[i];
		}
		delete[] this->_elements;
		this->_elements = nullptr;
		this->_elements = copy_arr;
	}
}

/*Resets the array to n*/
void Vector::resize(int n)
{
	if (n <= this->_capacity)
	{
		this->_size = n;
	}
	else
	{
		reserve(n);
	}
}
/*Sets value in all accessible vector parts*/
void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}
/*Resets the array to n
Sets value in all accessible vector parts*/
void Vector::resize(int n, const int& val)
{
	resize(n);
	assign(val);
}



/*Operator =   ,The big 3 */
Vector& Vector::operator=(const Vector& other)
{
	if (this == &other)
	{
		return *this;
	}

	delete[] this->_elements;
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;
	this->_elements = new int[other._capacity];
	for (int i = 0; i<this->_size; i++)
	{
		this->_elements[i] = other._elements[i];
	}
	return *this;
}
/*Copy c�tor */
Vector::Vector(const Vector& other)
{
	*this = other; // uses copy operator - copy other object to this object
}


/*Allows direct access to a particular index*/
int& Vector::operator[](int n) const
{
	if (n > this->_capacity)
	{
		std::cout << "Exceeding array boundaries" << std::endl;
		n = 0;
	}
	return this->_elements[n];
}
